# README For MyTweet Android App #


![MyTweet.JPG](https://bitbucket.org/repo/Xxno5L/images/1034313775-MyTweet.JPG)
![MyTweet-Activity.JPG](https://bitbucket.org/repo/Xxno5L/images/1141758961-MyTweet-Activity.JPG)

### What is this repository for? ###

* Quick summary this is an android application that mimics the functionality of Twitter, with login and signup features and the ability to post tweets to a time line.
* Version v23

### How do I get set up? ###

* Summary of set up, Clone the repo [https://austincunningham@bitbucket.org/austincunningham/mytweet.git](https://austincunningham@bitbucket.org/austincunningham/mytweet.git)
* Configuration open the app in Android Studio which can be download at [Android Studio](https://developer.android.com/studio/index.html), build to android SDK 23, for information on Android development and Android Studio see the official documentation at [Android Studio User Guide](https://developer.android.com/studio/intro/index.html) and [Android API Documentation](https://developer.android.com/guide/index.html). 
* Dependencies this application when compiled will run on android devices that have between SDK 16 and 23.
* Database configuration none needed installs sqlite DB as part of the apk
* Deployment instructions generate a signed APK file and copy to Android device 
![signed-APK.JPG](https://bitbucket.org/repo/Xxno5L/images/3481279435-signed-APK.JPG)

### Who do I talk to? ###

* Repo owner or admin austincunningham@oceanfree.net